package com.ashishmhalankar.calculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    EditText edFirstValue, edSecondValue;
    Button btnAdd, btnMinus, btnMultiplication, btnDivide;
    TextView tvAnswer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        edFirstValue = findViewById(R.id.edFirstValue);
        edSecondValue = findViewById(R.id.edSecondValue);

        btnAdd = findViewById(R.id.btnAdd);
        btnMinus = findViewById(R.id.btnMinus);
        btnMultiplication = findViewById(R.id.btnMultiplication);
        btnDivide = findViewById(R.id.btnDivide);

        tvAnswer = findViewById(R.id.tvAnswer);

        btnAdd.setOnClickListener(this);
        btnMinus.setOnClickListener(this);
        btnDivide.setOnClickListener(this);
        btnMultiplication.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAdd:
                addition();
                break;
            case R.id.btnMinus:
                subtraction();
                break;
            case R.id.btnMultiplication:
                multiplication();
                break;
            case R.id.btnDivide:
                division();
                break;

        }
    }

    Boolean isValid() {

        if (edFirstValue.getText().toString().isEmpty()) {
            edFirstValue.setError("Enter First Value");
            return false;
        }

        if (edSecondValue.getText().toString().isEmpty()) {
            edSecondValue.setError("Enter Second Value");
            return false;
        }

        return true;
    }



    void addition(){
        if (isValid()){
            int firstValue=Integer.valueOf(edFirstValue.getText().toString());
            int secondValue=Integer.valueOf(edSecondValue.getText().toString());

            int answer =firstValue+secondValue;

            tvAnswer.setText("Your Addition is :"+String.valueOf(answer));
        }
    }
    void subtraction(){
        if (isValid()){
            int firstValue=Integer.valueOf(edFirstValue.getText().toString());
            int secondValue=Integer.valueOf(edSecondValue.getText().toString());

            int answer =firstValue-secondValue;

            tvAnswer.setText("Your Subtraction is :"+String.valueOf(answer));
        }
    }

    void multiplication(){
        if (isValid()){
            int firstValue=Integer.valueOf(edFirstValue.getText().toString());
            int secondValue=Integer.valueOf(edSecondValue.getText().toString());

            int answer =firstValue*secondValue;

            tvAnswer.setText("Your Multiplication is :"+String.valueOf(answer));
        }
    }
    void division(){
        if (isValid()){
            int firstValue=Integer.valueOf(edFirstValue.getText().toString());
            int secondValue=Integer.valueOf(edSecondValue.getText().toString());

            int answer =firstValue/secondValue;

            tvAnswer.setText("Your Division is :"+String.valueOf(answer));
        }
    }
}
